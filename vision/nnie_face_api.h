#ifndef __NNIE_FACE_H__
#define __NNIE_FACE_H__


#ifdef __cplusplus
#if __cplusplus
extern "C"{
#endif
#endif /* __cplusplus */

/******************************************************************************
* function : Retinaface Detector func
******************************************************************************/

void NNIE_FACE_DETECTOR_INIT(char *pcModelName, float threshold, int isLog);
void NNIE_FACE_DETECTOR_GET(char *pcSrcFile);
void NNIE_FACE_DETECTOR_RELEASE(void);
void FACE_DETECTOR_PARAM_DEINIT();
HI_S32 SAMPLE_SVP_NNIE_Cnn_Deinit(SAMPLE_SVP_NNIE_PARAM_S *pstNnieParam, SAMPLE_SVP_NNIE_MODEL_S* pstNnieModel);
HI_S32 SAMPLE_SVP_NNIE_Cnn_ParamInit(SAMPLE_SVP_NNIE_CFG_S* pstNnieCfg, SAMPLE_SVP_NNIE_PARAM_S *pstCnnPara);
void FACE_DETECTOR_PARAM_INIT(float threshold, int isLog);
HI_S32 SAMPLE_SVP_NNIE_Forward(SAMPLE_SVP_NNIE_PARAM_S *pstNnieParam,
    SAMPLE_SVP_NNIE_INPUT_DATA_INDEX_S* pstInputDataIdx,
    SAMPLE_SVP_NNIE_PROCESS_SEG_INDEX_S* pstProcSegIdx,HI_BOOL bInstant);
HI_S32 SVP_NNIE_MNET(SAMPLE_SVP_NNIE_PARAM_S *pstNnieParam);
HI_U32 SVP_NNIE_MNET_Retina(SAMPLE_SVP_NNIE_PARAM_S *pstNnieParam, float *cls_threshold, pt_result *point_results, float *nms_threshold, scale_w_h *scales);

/******************************************************************************
* function : Face Recognition func
******************************************************************************/
void NNIE_FACE_EXTRACTOR_INIT(char *pcModelName);
void NNIE_FACE_NNIE_EXTRACTOR_GET(char *pcSrcFile, float *feature_buff);
void NNIE_FACE_EXTRACTOR_RELEASE(void);
#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* __cplusplus */


#endif /* __NNIE_FACE_H__ */
