#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <signal.h>
#include <pthread.h>
#include <sys/prctl.h>
#include <math.h>
//#include "config.h"
#include "hi_common.h"
#include "hi_comm_sys.h"
#include "hi_comm_svp.h"
#include "sample_comm.h"
#include "sample_comm_svp.h"
#include "sample_comm_nnie.h"
//#include "nnie_face_api.h"
#include "sample_svp_nnie_software.h"
#include "sample_comm_ive.h"
#include "hi_type.h"

#define PriorBox_Num 9690
#define Node_Num 2
#define INPUT_SIZE 300.0f
#define LONGTH 6
#define a1_LONGTH 5
#define a2_LONGTH 4

#define ssd_MAX(a,b)  ((a) < (b) ? (b) : (a))
#define ssd_MIN(a,b)  ((a) > (b) ? (b) : (a))

#define SSD_ALIGN_UP(x, a)           ( ( ((x) + ((a) - 1) ) / a ) * a )
#define SSD_ALIGN_DOWN(x, a)         ( ( (x) / (a)) * (a) )


typedef struct PriorBox{
	float anchor_cx;
	float anchor_cy;
	float anchor_w;
	float anchor_h;
}PriorBox;

typedef struct PredBbox{
	float bbox_x_left;
	float bbox_y_left;
	float bbox_x_right;
	float bbox_y_right;
}PredBbox;

typedef struct Node{
	PredBbox bbox;
	int confidence;
	struct Node* next;
}Node;


int priorbox_init(PriorBox *priorbox);
void get_pred_bbox(SAMPLE_SVP_NNIE_PARAM_S *pstNnieParam, PriorBox *priorbox, Node *predbbox_head_node, float cls_threshold_);
void free_bbox(Node *predbbox);
void pred_bbox_sort(Node *predbbox);
void pred_bbox_nms(Node *predbbox, float nms);
int coordinate_clip_int(int down, int up, int input);



